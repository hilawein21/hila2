#lang racket

(require "utils.rkt")
(require "relational-algebra.rkt")
(require "sql-interpreter.rkt")
(require "sql-asp.rkt")

(define DB2
  '(
    (Students
     (S_StudentId S_Name S_Course)
     (0 'Avi 'CS330)
     (1 'Yosi 'CS142)
     (2 'Sarah 'CS630))
    (Students2
     (S_StudentId S_Name S_Course)
     (3 'David 'CS350)
     (4 'Moshe 'PSY342)
     (5 'Eli 'ENG632))
    (Students3
     (S_StudentId S_Name S_Course)
     (3 'David 'CS350)
     (12 'Moshe 'PSY342)
     (5 'Eli 'ENG632))
    (Students4
     (S_StudentId S_Name S_Course)
     (0 'David 'CS350)
     (12 'Moshe 'PSY342)
     (5 'Eli 'ENG632))
    ))

(define qu0 '(select (S_StudentId S_Name S_Course) from (Students union Students2) where (S_StudentId = 0)))
(define qu1 '(select (S_StudentId S_Name S_Course) from (Students union Students2) where (S_StudentId = 3)))
(define qu2 '(select (S_StudentId S_Name S_Course) from (Students union Students2)))
(define qu3 '(select (S_Course) from (Students union Students2)))
(define qu4 '(select (S_StudentId S_Course) from (Students2 union Students) where (S_StudentId > 2)))
(define qu5 '(select (S_Course S_StudentId) from (Students2 union Students3) where (S_StudentId > 2)))
(define qu6 '(select (S_Course S_StudentId) from (Students2 union (Students3 union Students)) where (S_StudentId > 0)))
(define qu7 '(select (S_Name) from ((Students2 union Students4) union (Students3 union Students)) where (S_StudentId > 0)))

;; sql-interpreter tests
(define (union-tests)
  (run-tests
   (test (eval-query (parse-query qu0) DB2) => '((S_StudentId S_Name S_Course) (0 'Avi 'CS330)))
   (test (eval-query (parse-query qu1) DB2) => '((S_StudentId S_Name S_Course) (3 'David 'CS350)))
   (test (eval-query (parse-query qu2) DB2) => '((S_StudentId S_Name S_Course) (0 'Avi 'CS330) (1 'Yosi 'CS142) (2 'Sarah 'CS630) (3 'David 'CS350) (4 'Moshe 'PSY342) (5 'Eli 'ENG632)) )
   (test (eval-query (parse-query qu3) DB2) => '((S_Course) ('CS330) ('CS142) ('CS630) ('CS350) ('PSY342) ('ENG632)))
   (test (eval-query (parse-query qu4) DB2) => '((S_StudentId S_Course) (3 'CS350) (4 'PSY342) (5 'ENG632)))
   (test (eval-query (parse-query qu5) DB2) => '((S_Course S_StudentId) ('CS350 3) ('PSY342 4) ('ENG632 5) ('CS350 3) ('PSY342 12) ('ENG632 5)))
   (test (eval-query (parse-query qu6) DB2) => '((S_Course S_StudentId) ('CS350 3) ('PSY342 4) ('ENG632 5) ('CS350 3) ('PSY342 12) ('ENG632 5) ('CS142 1) ('CS630 2)))
   (test (eval-query (parse-query qu7) DB2) => '((S_Name) ('David) ('Moshe) ('Eli) ('Moshe) ('Eli) ('David) ('Moshe) ('Eli) ('Yosi) ('Sarah)))
   ))

(display "\n\n===================\n")
(display "union tests:\n")
(display "===================\n")
(display "   sql-interpreter tests:\t")
(union-tests)